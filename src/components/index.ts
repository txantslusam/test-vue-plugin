import Vue from 'vue';

import Banner from './Banner.vue';
import Creator from './Creator.vue';

const Components = {
  Banner,
  Creator,
}

Object.entries(Components).forEach(([name, component]) => {
  Vue.component(name, component);
})

export default Components;
export { Banner, Creator };
